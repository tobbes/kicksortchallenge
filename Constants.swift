//
//  Constants.swift
//  kicksortTest
//
//  Created by Tobias Ednersson on 18/01/16.
//  Copyright © 2016 Tobias Ednersson. All rights reserved.
// Constants used in the project. 
// These could alternatively be put into info.plist

import UIKit

struct Constants {
    static let urlString = "http://coordinatetest.azurewebsites.net/api/Map";
    static let USER = "tobiasednersson"
    static let PASSWORD = "791105"
    static let STATUS_OK =  200
    //Number of times to try login before giving up
    static let LOGIN_TRIES = 1
}
