//
//  ViewController.swift
//  kicksortTest
//
//  Created by Tobias Ednersson on 15/01/16.
//  Copyright © 2016 Tobias Ednersson. All rights reserved.
//

import UIKit
import MapKit
class ViewController: UIViewController,NSURLSessionDataDelegate,MKMapViewDelegate {
//Constants in
    
    @IBOutlet var map: MKMapView!
       override func viewDidLoad() {
        super.viewDidLoad()
        self.setUp()
    // Do any additional setup after loading the view, typically from a nib.
    }

    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    
    
    
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didReceiveChallenge challenge: NSURLAuthenticationChallenge, completionHandler: (NSURLSessionAuthChallengeDisposition, NSURLCredential?) -> Void) {
        
        let credentials = NSURLCredential(user: Constants.USER, password: Constants.PASSWORD, persistence: NSURLCredentialPersistence.ForSession)
        if(challenge.previousFailureCount > Constants.LOGIN_TRIES) {
            completionHandler(NSURLSessionAuthChallengeDisposition.CancelAuthenticationChallenge,credentials)
        }
        else {
        completionHandler(NSURLSessionAuthChallengeDisposition.UseCredential,credentials)
        }
        
        
    }
    
    
    
    
    
    
    private func setUp() -> Void {
        self.map.delegate = self;
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        config.HTTPAdditionalHeaders = ["Accept" : "application/json"]
        config.allowsCellularAccess = true
        
        let session = NSURLSession(configuration: config, delegate: self, delegateQueue: nil)
        session
        
        let completionHandler = {(data:NSData?,response:NSURLResponse?,error:NSError?) -> Void in
            
            if(error != nil) {
                self.reportConnectionError((error?.code)!)
                print("ERROR: \(error!.localizedDescription)")
                return;
            }
            
            if(response != nil) {
                 let code =  (response! as! NSHTTPURLResponse).statusCode
             
                if(code == Constants.STATUS_OK) {
                    let locations:[CLLocation] =  self.JsonToCoordinates(data!)
                
                    self.plot(locations)
                    let center = self.findCenter(locations)
                    
                  let toPlot = self.findNeighbours(locations)
                   self.drawLines(toPlot)
                   self.fillFigure(locations)
                    
                    
                    //let size = self.findSize(locations, center: center)
                    let region = MKCoordinateRegionMakeWithDistance(center,1500 ,1500)
                    dispatch_async(dispatch_get_main_queue(), {self.map.setRegion(region, animated: true)})
                }
                else {
                    
                    self.reportConnectionError(code)
                    return
                }
                
               
                
            }
            
            
        }
        
        
        let url = NSURL(string: Constants.urlString)
        let task = session.dataTaskWithURL(url!,completionHandler: completionHandler)
        
        
        task.resume()
    }
    
    
    
    
    //Finds the centre of the cluster of coordinates
    func findCenter(locations:[CLLocation]) ->CLLocationCoordinate2D {
        var sumLat:Double = 0
        var sumLong:Double = 0
        for loc in locations{
        sumLat += loc.coordinate.latitude
        sumLong += loc.coordinate.longitude
        }
        return CLLocationCoordinate2D(latitude: sumLat/Double(locations.count),longitude: (sumLong/Double(locations.count)))
        
    }
    
    
    
    
    func reportConnectionError(code:Int) ->Void {
        let message = UIAlertController(title: "Failed loading data", message: "Errorcode: \(code)",preferredStyle: UIAlertControllerStyle.Alert)
        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action:UIAlertAction) -> Void in
            message.dismissViewControllerAnimated(true, completion: nil)
        })
        message.addAction(action)
        dispatch_async(dispatch_get_main_queue(),{self.presentViewController(message, animated: true, completion: nil)})
        print("FAILED: \(code)")
    }
    
    
    
   
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        var view:MKAnnotationView? = self.map.dequeueReusableAnnotationViewWithIdentifier("MyAnnotView")
        if(view == nil) {
            view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "MyAnnotView")
        }
        
        else {
            view!.annotation = annotation
        }
        
        (view as! MKPinAnnotationView).pinTintColor = UIColor.redColor()
        return view
    }
    
    func JsonToCoordinates(data:NSData) -> [CLLocation]{
        var locations:[CLLocation] = []
        do {
            
            let json = try NSJSONSerialization.JSONObjectWithData(data, options: [])
            let coordinates = json as! [[String:Double]]
            for coordinate in coordinates {
                let loc = CLLocation(latitude: coordinate["Lat"]!, longitude: coordinate["Lng"]!)
                locations.append(loc)
            }
        }
        catch {

            let message = UIAlertController(title: "FAILED SETUP", message: "Could not parse coordinates ", preferredStyle: UIAlertControllerStyle.Alert)
            let action = UIAlertAction(title: "FAILED TO PLOT", style: UIAlertActionStyle.Default, handler: {(action:UIAlertAction) in message.dismissViewControllerAnimated(true, completion: nil)})
            message.addAction(action)
            dispatch_async(dispatch_get_main_queue(), {self.presentViewController(message, animated: true, completion: nil)})
        
        }
        return locations;
            }
    

    
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        var renderer:MKOverlayRenderer = MKOverlayRenderer(overlay: overlay)
        
        switch(overlay)
        {
        case is MKPolygon :  renderer = MKPolygonRenderer(overlay: overlay)
        (renderer as!MKPolygonRenderer).lineWidth = 0.001
        renderer.alpha = 0.5;
        (renderer as! MKPolygonRenderer).strokeColor = UIColor.blackColor()
        (renderer as! MKPolygonRenderer).fillColor = UIColor.blueColor()
        break;
        case is MKPolyline: renderer = MKPolylineRenderer(overlay: overlay)
        (renderer as! MKPolylineRenderer).lineWidth =  10.0;
        (renderer as! MKPolylineRenderer).strokeColor = UIColor.whiteColor()
        break

        default: renderer = MKOverlayRenderer(overlay: overlay)
        }
        return renderer
    }
    
    
    
    private func fillFigure(locations:[CLLocation]) ->Void {
        
        var coordinates:[CLLocationCoordinate2D] = []
        
        for loc in locations {
            coordinates.append(loc.coordinate)
        }
        
        let polygon_k = MKPolygon(coordinates: &coordinates , count: coordinates.count)
        
       
        dispatch_async(dispatch_get_main_queue(), {self.map.addOverlay(polygon_k)})
    }
    
    
    
    
    
    private func findNeighbours(locations:[CLLocation]) ->[[CLLocation]]{
        var neighbours:[[CLLocation]] = []
        var closest:[CLLocation] = []

        for location in locations {
            closest = []
            var mindist1:Double  = 0
            var mindist2:Double = 0
            closest.append(location)
            for n in locations {
                //Do not draw a line from a location to itself!!
                if(n == location) {
                    continue
                }
                if(closest.count == 1) {
                    
                    closest.append(n)
                    mindist1 = n.distanceFromLocation(location)
                    continue
                }
                
                
                else if(closest.count == 2) {
                    mindist2 = n.distanceFromLocation(location)
                    closest.append(n)
                    continue
                }
                
               let dist = n.distanceFromLocation(location)
               
                if(dist < mindist1) {
                    
                    if(closest[1].distanceFromLocation(location) < mindist2) {
                        closest[2] = closest[1]
                    }
                    closest[1] = n
                    mindist1 = dist
                }
                
                else if(dist < mindist2) {
                    
                    mindist2 = dist;
                    closest[2] = n
                }
                
                
                
                
            }
        
            neighbours.append(closest)
        }
        
            
        return neighbours
            
        }


    private func plot(locations:[CLLocation]) {
    
    for loc in locations {
    
        let pin = MKPointAnnotation()
        pin.coordinate = loc.coordinate
        
        let b = {
                self.map.addAnnotation(pin)
        }
        dispatch_async(dispatch_get_main_queue(), b)
        
        
        
        
        
        
        }
    }
    


    
    
    private func drawLines(coordinates:[[CLLocation]]) -> Void{
 
        
        for c in coordinates {
            var pair:[CLLocationCoordinate2D];
            var pair2:[CLLocationCoordinate2D]
            
            
            pair = [c[0].coordinate, c[1].coordinate]
            pair2 = [c[0].coordinate, c[2].coordinate]
            let line1 = MKPolyline(coordinates: &pair,count: 2)
            
            
            
            let line2 = MKPolyline(coordinates: &pair2,count: 2)
            
            let b = {self.map.addOverlay(line1)
                    self.map.addOverlay(line2)
            }
            dispatch_async(dispatch_get_main_queue(), b)
            
            
        }
            
            
            
}
}

